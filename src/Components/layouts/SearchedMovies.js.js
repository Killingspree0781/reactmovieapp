import React from 'react'
import Movie from './Movie'

const SearchedMovies = (props) => {

    const renderMovie = (movie) => {
      return <Movie movie={movie} notMyMoviesList={true} onClick={(movie)=>props.onClick(movie)}/>;
    }
      return (
        <div className="container">
          <div className="jumbotron">
            <form class="form-inline justify-between">
              <input class="form-control" type="text" onChange={(event)=>props.onChange(event.target.value)}/> 
              <button class="btn btn-primary" type="submit">
                Search
              </button>
            </form>
            <h1 className='text-center'>Searched Movies.......</h1>
          </div>
          <div class="row">
          {props.movies.map(movie => renderMovie(movie))}
          </div>
        </div>
      );
  }

export default SearchedMovies;