import React from 'react'
import Movie from './Movie'

const MyMovies = (props) => {

    const renderMovie = (movie) => {
      return <Movie movie={movie} notMyMoviesList={false} onClick={(movie)=>props.onClick(movie)}/>;
    }
      return (
        <div className="container">
            <div className="jumbotron">
            <h1 className='text-center'>My Movies.......</h1>
            </div>
            <div class="row">
            {props.movies.map(movie => renderMovie(movie))}
            </div>
        </div>
      );
  }



export default MyMovies;