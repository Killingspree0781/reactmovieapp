import React, { Component } from "react";
import SearchedMovies from "./SearchedMovies.js";
import MyMovies from "./MyMovies";
import Navbar from "./Navbar";
export class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notMyMoviesList: true,
      movies: [],
      mymovies: [],
    };
  }
  componentDidMount(name) {
    if(name){
    fetch("http://www.omdbapi.com/?apikey=3fda8513&type=movie&s=" + name)
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            movies: result.Search ? result.Search : [],
          });
        }
      );
    }
  }

  addToMyMovies(movie){
    if(this.state.mymovies.length<10){
      this.setState({
        mymovies: this.state.mymovies.concat([movie]),
      });
    }
    else{
      alert("you can only add 10 movies. Delete some movies if you wnat to add");
    }
  }

  deleteFromMyMovies(movie){
    var mymovies=this.state.mymovies.slice();
    mymovies.splice(mymovies.indexOf(movie),1);
    this.setState({
      mymovies: mymovies
    });
  }

  render() {
    return (
      <div>
        <Navbar
          onSelectMyMovies={() => this.setState({ notMyMoviesList: false })}
          onSelectSearchMovies={() => this.setState({ notMyMoviesList: true })}
        />
        <div className=''>
          {this.state.notMyMoviesList ? (
            <SearchedMovies
              movies={this.state.movies}
              onClick={(movie) =>this.addToMyMovies(movie)}
              onChange={(name) => this.componentDidMount(name)}
            />
          ) : (
            <MyMovies
              movies={this.state.mymovies}
              onClick={(movie) =>this.deleteFromMyMovies(movie)}
            />
          )}
        </div>
      </div>
    );
  }
}
export default Page;
