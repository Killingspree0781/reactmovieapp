import React from "react";

const Movie = (props) => {
    // var movieData;
    // var showMore=false;
    // fetch("http://www.omdbapi.com/?apikey=3fda8513&type=movie&i=" + props.movie.imdbID)
    // .then((res) => res.json())
    // .then(
    //     (result) =>movieData=result
    // );
  return (
    <div class='col-md-4'>
      <div class='card'>
        <img
          class='card-img-top'
          alt='Bootstrap Thumbnail Second'
          src={props.movie.Poster}
        />
        <div class='card-block'>
          <h5 class='card-title'>{props.movie.Title}</h5>
          <p class='card-text'>{props.movie.Year}</p>
          <p>
            {props.notMyMoviesList ? (
              <button
                class='btn btn-primary'
                onClick={() => props.onClick(props.movie)}
              >
                Add
              </button>
            ) : (
              <button
                class='btn btn-primary'
                onClick={() => props.onClick(props.movie)}
              >
                Delete
              </button>
            )}
          </p>
          {/* <p>
              Plot: {movieData.Plot}
              <button class='btn btn-success'
                onClick={showMore = false}>Show Less...</button>
          </p> */}
          {/* :
          <p>
              <button class='btn btn-success'
                onClick={showMore = true}>Show More...</button>
          </p> */}
        </div>
      </div>
    </div>
  );
};

export default Movie;
