import React from 'react'

const Navbar =  (props)=> {
        return (
            <nav class="navbar navbar-expand-lg navbar-light bg-light"> 
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="navbar-toggler-icon"></span>
				</button> <a class="navbar-brand" href="#">Publicis Sapient</a>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="navbar-nav ml-md-auto">
						<li class="nav-item active">
							<a class="nav-link" onClick={props.onSelectSearchMovies}>Search Movies</a>
						</li>
						<li class="nav-item active">
							<a class="nav-link" onClick={props.onSelectMyMovies}>My Movies</a>
						</li>
					</ul>
				</div>
			</nav>
        )
}

export default Navbar
