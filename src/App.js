import React from 'react';
import './App.css';
import Page from './Components/layouts/Page';


function App() {
  return (
    <div className="App">
      <Page/>
    </div>
  );
}

export default App;
